#!/bin/bash

echo "Login to GitLab registry"
docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
echo "Building Docker image"
docker build -t $CI_REGISTRY_IMAGE/backup_container:latest docker/
echo "Pushing image to registry"
docker push $CI_REGISTRY_IMAGE/backup_container:latest