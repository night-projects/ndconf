# Net Device Config Saver

For saving network device configs to ease sleep at night.

This is a small side project that I use to backup my personal gear. Over time I may add things as my networks evolve, depending on the remaining time I have at night.

Currently this only supports MikroTik RouterOS, and contains limited authentication options.
## Requirements

### Supported devices to be backed up.
- RouterOS device.
  - Only SSH password authentication is supported currently.
  - Runs version 7.1.5 or higher.
### For running this project
- Any compatible Linux distribution
  - Only tested with [Ubuntu](https://ubuntu.com/download/server) 20.04 LTS.
  - Should support Python 3.8 or newer.
- [pip](https://pip.pypa.io/en/stable/installation/)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html) & its dependencies.
  - Also requires [paramiko](https://www.paramiko.org/installing.html) to be installed separately.
  - Only tested with version 5, or 2.11 core.
- [sshpass](https://sourceforge.net/projects/sshpass/).
  - Usually can be installed via apt/yum/dnf.

...or you can build the Docker image that's located in `docker/` which would contain all needed software to run.

### For GitLab backups

- Separate, private repository for storing the text backups.
  - Also requires adding deploy keys to the repo.
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- GitLab runner

## Getting started

1. Download this repository.<br>
Easiest way to use this is to fork this project to your own namespace. You can fork this project [here](https://gitlab.com/night-projects/ndconf/-/forks/new). Otherwise, you can download it manually or clone it, and add the files manually to your own repo, however you want to do it. <br>
2. Create the inventory.<br>
Create a file named `hosts.yaml` in the `ansible/` directory, and populate it with the following information below while adjusting it to your needs. See the next section on creating inventory/hosts file for additional information.<br>
```
all:
  hosts:
  # Name your hosts to your liking here.
    router:
      # Set the IP address to connect to.
      ansible_host: 1.2.3.4
      # Set SSH port if it's different than 22.
      # ansible_port: 22
      # If not wanting to pull username and password externally, you can manually set them here (dangerous).
      # ansible_user: 
      # ansible_password:
  vars:
    ansible_connection: ansible.netcommon.network_cli
    ansible_network_os: community.routeros.routeros
    # If planning to use GitLab CI/CD to execute backups, you can adjust the vars below. Otherwise, set gl_sync to false.
    gl_repo: "BACKUP_REPO"
    gl_dir: "BACKUP_DIR"
    ros_password: "NETPASS"
    ros_user: "NETUSER"
    gl_repo_key: "REPO_KEY"
    ros_backup_pw: "ROS_BACKUP_PW"
    gl_sync: true
    backup_sensitive: false
``` 
3. Execute (local) or skip.<br>
If running this locally or manually, just execute `run.sh` and you're done. <br>
For using GitLab CI/CD and having config uploaded to GitLab, proceed to the next step.
4. Create backup repo.<br>
Create a private repository for your configs to be stored in. Ideally this is separate from the forked/cloned repo that executes this project.
5. Create SSH Key.<br>
Create a separate SSH key purely for the purpose to be used as a deploy key. Add the public key to the backup repo as a deploy key.
6. Add GitLab CI/CD Variables.<br>
Add the needed variables based on the names used in `hosts.yaml`. Within your forked/cloned repo (NOT the one used for storing backups), go to `Settings->CI/CD->Variables` to add your variables. If using the default values from step 2, simply add the variables as follows:<br>
  - Key: `BACKUP_REPO`<br>
    Value: `[git clone URL value]`, example: `git@gitlab.com:my-user/my-backup-repo.git`
  - Key: `BACKUP_DIR`<br>
    Value: `[directory name of the backup repo]`
  - Key: `NETPASS`<br>
    Value: `[device password]`<br>
    NOTE: It is highly recommended to have this masked.
  - Key: `NETUSER`<br>
    Value: `[device username]`<br>
    NOTE: Would also recommend to have this masked, because why not.
  - Key: `REPO_KEY`<br>
    Value: `[SSH private key from step 5]`<br>
    NOTE: This should be set as a `File` type, and should be copied EXACTLY how it was generated.
  - Key: `ROS_BACKUP_PW`<br>
    Value: `[password string of your choice]`
    NOTE: Recommended to set this as masked as well.

7. Setup gitlab-ci.yml.<br>
Rename/copy `gitlab-ci.yml.example` to `.gitlab-ci.yml` for a quick & basic pipeline. 
8. Execute. <br>
After committing your changes and pushing to your own repo, the pipeline may start executing. However, since the Docker image might not exist in your registry, you may need to run the pipeline manually and adding variable `DOCKER_BUILD` to `true`. From there, any changes the `docker/` directory or `build_docker.sh` will trigger the image to be rebuilt and uploaded.<br>
To run manually, set the variable `BACKUP_NETWORK` to `true` to start executing. Any changes to the inventory will also trigger the pipeline to run. As well, running the pipeline on a schedule will also execute.
 
## Creating the inventory/hosts file
It is recommended to be familiar with creating/editing Ansible inventory files. For this project, Ansible tries to run against hosts under `all` group.<br>
Below is a list of variables that can be set for hosts.

- ansible_host: IP address/URL to reach the device.<br>
  Default: None
- ansible_port: SSH port, if different than 22.<br>
  Default: 22
- ansible_connection: Connection type. For RouterOS this should be `ansible.netcommon.network_cli` for now.
  Default: None
- ansible_network_os: Defines the OS. For RouterOS, this should  `community.routeros.routeros`.
  Default: None
- ansible_user: Username to authenticate with. This can pulled from another variable rather than manually specifying in `hosts.yaml`.
  Default: None
- ansible_password: Password to authenticate with. This can be pulled from another variable, not suggested to have defined in `hosts.yaml` unless debugging/testing.
  Default: None
- ros_user: Environmental variable name to lookup that would contain the username to authenticate with.<br>
  Default: admin
- ros_password: Environmental variable name to lookup that would contain the password to authenticate with.<br>
  Default: None/blank
- ros_facts_subset: Facts subset to obtain, as defined [here](https://docs.ansible.com/ansible/devel/collections/community/routeros/facts_module.html#ansible-collections-community-routeros-facts-module).<br>
  Default: !config
- ros_backup_pw: Environmental variable name to lookup that contains the password to use for encrypting the full device backups (different from `/export`).<br>
  Default: encrypted
- backup_sensitive: Boolean on whether to obtain sensitive configuration from `/export`.<br>
  Default: false
- gl_server: GitLab server instance to use.<br>
  Default: gitlab.com
- gl_sync: Boolean of whether or not configs should be backed up to GitLab.<br>
  Default: false
- gl_repo_key: Environmental variable name to lookup that contains the SSH private key for connecting to the backup repo.<br>
  Default: None
- gl_repo: Environmental variable name to lookup that contains the SSH git clone URL, such as `git@gitlab.com:my-user/my-backup-repo.git`.<br>
  Default: None
- gl_dir: Environmental variable name to lookup that contains the name of the folder of the cloned backup repo.<br>
  Default: None 

### Example inventory

An example similar to what I personally use:
```
all:
  children:
    home:
      hosts:
        router:
          ansible_host: 192.168.1.1
          ansible_port: 2222
        sw1:
          ansible_host: 192.168.1.3
        sw2:
          ansible_host: 192.168.1.4
      vars:
        ansible_connection: ansible.netcommon.network_cli
        ansible_network_os: community.routeros.routeros
        gl_repo: "BACKUP_REPO"
        gl_dir: "BACKUP_DIR"
        ros_password: "NETPASS"
        ros_user: "NETUSER"
        gl_repo_key: "REPO_KEY"
        ros_backup_pw: "ROS_BACKUP_PW"
        gl_sync: true
```
## Reporting issues & requests

Head over to [Issues](https://gitlab.com/night-projects/ndconf/-/issues) and create a new issue for issues found or possible features you may want to see. Please note that this project is handled with a low priority manner, and the scope of this project currently is just to create some basic backups for network devices.
## Additional documentation

This project primarily uses [Ansible](https://docs.ansible.com/ansible/latest/index.html), and at the very least it would be good to understand how to build an [inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#intro-inventory). <br>
You can also read [here](https://docs.ansible.com/ansible/latest/user_guide/index.html#getting-started) on how to get started with Ansible as well.

This project also uses [Docker](https://docs.docker.com/), and the following docs may be helpful:
- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
- [docker build](https://docs.docker.com/engine/reference/commandline/build/)
- [docker exec](https://docs.docker.com/engine/reference/commandline/exec/)

If using GitLab CI/CD and to understand how to create & edit a project's pipeline, see GitLab's docs [here](https://docs.gitlab.com/ee/ci/). Specific documentation for installing GitLab runner can be found [here](https://docs.gitlab.com/runner/install/).